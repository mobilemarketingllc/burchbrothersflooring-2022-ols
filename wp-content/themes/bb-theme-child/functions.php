<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);

});


//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


add_filter('gform_form_args', 'no_ajax_on_all_forms', 10, 1);
function no_ajax_on_all_forms($args){
    $args['ajax'] = false;
    return $args;
}



//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;
    $instock = get_post_meta( $post->ID , "in_stock");

    if (is_singular( 'luxury_vinyl_tile' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-waterproof-luxury-vinyl-products/',
                'text' => 'In Stock Waterproof Luxury Vinyl Products',
            );
        }else{    
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/luxury-vinyl/',
                'text' => 'Luxury Vinyl',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/luxury-vinyl/products/',
                'text' => 'Luxury Vinyl Products',
            );
        }
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    return $links;
}

function storelocation_parameter($arg)
{
    
    $website = json_decode(get_option('website_json'));
       
    
    for ($i=0;$i < count($website->locations);$i++) {

        $location_name = isset($website->locations[$i]->name) ? $website->locations[$i]->name:"";

        $location_ids = isset($website->locations[$i]->id) ? $website->locations[$i]->id:"";
       
        if($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name !=''){

            
            if (in_array(trim($location_name), $arg) || in_array(trim($location_ids), $arg)){
                
                if (in_array("city", $arg)) {
                            
                    $locations = "<span class='city_loc'>".$website->locations[$i]->city."</span>";
                }

                if (in_array("state", $arg)) {

                    $locations = "<span class='state_loc'>".$website->locations[$i]->state."</span>";
                 }   
            } 

        }        
    }

    
    return $locations;
}
    add_shortcode('storelocation', 'storelocation_parameter');