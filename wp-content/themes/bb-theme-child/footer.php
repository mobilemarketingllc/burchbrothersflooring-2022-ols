<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>  role="contentinfo">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>
<script type="application/ld+json">{"@context":"https://schema.org","@type":"LocalBusiness","name":"Burch Brothers Flooring","image":"https://burchbrothersflooring.com/wp-content/uploads/2022/07/burch-brothers-flooring-garner-nc.jpeg","@id":"","url":"https://burchbrothersflooring.com","telephone":"(919) 615-0022","priceRange":"$","address":{"@type":"PostalAddress","streetAddress":"182 Cleveland Crossing Dr #400","addressLocality":"Garner","addressRegion":"NC","postalCode":"27529","addressCountry":"US"},"geo":{"@type":"GeoCoordinates","latitude":35.6050496,"longitude":-78.56047819999999},"openingHoursSpecification":{"@type":"OpeningHoursSpecification","dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Friday"],"opens":"09:00","closes":"17:00"},"sameAs":["https://www.facebook.com/BurchBrothersFlooring/","https://www.instagram.com/burchbrothersflooring/"]}</script>
</body>
</html>
